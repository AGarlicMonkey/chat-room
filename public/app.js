new Vue({
    el: '#app',

    data: {
        ws: null, //The websocket
        newMsg: '', //Holds new messages to be sent to the server
        chatContent: '', //A running list of chat messages to be displayed on screen
        email: null, //Email address used for grabbing the avart
        username: null,
        joined: false, //True if email and usename have been set
    },

    created: function () {
        var self = this;
        this.ws = new WebSocket('ws://' + window.location.host + '/ws');
        this.ws.addEventListener('message', function (e) {
            var msg = JSON.parse(e.data);
            self.chatContent += '<div class="chip">'
                + '<img src="' + self.gravatarURL(msg.email) + '">'
                + msg.username
                + '</div>'
                + emojione.toImage(msg.message) + '<br/>';

            var element = document.getElementById('chat-messages');
            element.scrollTop = element.scrollHeight; //Scroll to bottom
        });
    },

    methods: {
        send: function () {
            if (this.newMsg != '') {
                this.ws.send(
                    JSON.stringify({
                        email: this.email,
                        username: this.username,
                        message: $('<p>').html(this.newMsg).text() //Strip out html
                    })
                );
                this.newMsg = '';
            }
        },

        join: function () {
            if (!this.email) {
                Materialize.toast('You must enter an email', 2000);
                return
            }
            if (!this.username) {
                Materialize.toast('You must choose a username', 2000);
                return
            }
            this.email = $('<p>').html(this.email).text();
            this.username = $('<p>').html(this.username).text();
            this.joined = true;
        },

        gravatarURL: function (email) {
            return 'http://www.gravatar.com/avatar/' + CryptoJS.MD5(email);
        }
    }
});