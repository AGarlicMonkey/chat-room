package main

import (
	"log"
	"net/http"

	"github.com/gorilla/websocket"
)

var clients = make(map[*websocket.Conn]bool) //Connected clients - TODO: Make array?
var broadcast = make(chan Message)

//Takes a HTTP connection and upgrades it to a WebSocket
var upgrader = websocket.Upgrader{}

type Message struct {
	Email    string `json:"email"`
	Username string `json:"username"`
	Message  string `json:"message"`
}

func main() {
	//Create a file server and tie that to "/"
	fileServer := http.FileServer(http.Dir("../public"))
	http.Handle("/", fileServer)

	//Set up WebSocket route
	http.HandleFunc("/ws", handleConnections)

	//Listen for incoming chat messages
	go handleMessages()

	//Start a server and log any errors
	log.Println("http server started on :8000")
	err := http.ListenAndServe(":8000", nil)
	if err != nil {
		log.Fatal("ListenAndServe", err)
	}
}

func handleConnections(writer http.ResponseWriter, request *http.Request) {
	//Upgrade initial request to a WebSocket
	webSocket, err := upgrader.Upgrade(writer, request, nil)
	if err != nil {
		log.Fatal(err)
	}
	defer webSocket.Close()

	//Register new client
	clients[webSocket] = true

	for {
		var msg Message

		//Read in a new message as JSON and map it to the message object
		err := webSocket.ReadJSON(&msg)
		if err != nil {
			log.Printf("error: %v", err)
			delete(clients, webSocket)
			break
		}

		//Send the message to the broadcast channel
		broadcast <- msg
	}
}

func handleMessages() {
	for {
		//Get a message from the broadcast channel
		msg := <-broadcast
		//Then send it out to all connected clients
		for client := range clients {
			err := client.WriteJSON(msg)
			if err != nil {
				log.Printf("error: %v", err)
				client.Close()
				delete(clients, client)
			}
		}
	}
}
